package com.wavemaker.jsondiff.sample;

import java.io.File;
import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import foodev.jsondiff.JacksonDiff;

/**
 * @author <a href="mailto:dilip.gundu@wavemaker.com">Dilip Kumar</a>
 * @since 28/4/15
 */
public class JsonCompare {

    public static void main(String[] args) throws IOException {
        JacksonDiff jacksonDiff = new JacksonDiff();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        Object diff = jacksonDiff
                .diff(mapper.readTree(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                                "swagger.json")),
                        mapper.readTree(
                                Thread.currentThread().getContextClassLoader().getResourceAsStream("swagger1.json")));

        mapper.writeValue(new File("target", "diff.json"), diff);
    }
}
